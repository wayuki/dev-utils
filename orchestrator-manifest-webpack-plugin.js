const path = require('path');

function isHotUpdateCompilation(assets) {
  return assets.length && assets.every((assetPath) => /\.hot-update\.js$/.test(assetPath));
}

function getAssets(compilation, filename, entryNames, seed) {
  const compilationHash = compilation.hash;

  const webpackPublicPath = compilation.mainTemplate.getPublicPath({ hash: compilationHash });
  const isPublicPathDefined = webpackPublicPath.trim() !== '';
  let publicPath = isPublicPathDefined
    ? webpackPublicPath
    : path.relative(path.resolve(compilation.options.output.path, path.dirname(filename)), compilation.options.output.path).split(path.sep).join('/');

  if (publicPath.length && publicPath.substr(-1, 1) !== '/') {
    publicPath += '/';
  }

  const assets = seed;

  const entryPointPublicPathMap = {};
  const extensionRegexp = /\.(js|css)(\?|$)/;
  for (let i = 0; i < entryNames.length; i += 1) {
    const entryName = entryNames[i];
    const entryPointFiles = compilation.entrypoints.get(entryName).getFiles();
    const entryPointPublicPaths = entryPointFiles.map((chunkFile) => publicPath + chunkFile);

    assets[entryName] = [];

    entryPointPublicPaths.forEach((entryPointPublicPath) => {
      const extMatch = extensionRegexp.exec(entryPointPublicPath);
      if (!extMatch) {
        return;
      }

      if (entryPointPublicPathMap[entryPointPublicPath]) {
        return;
      }

      entryPointPublicPathMap[entryPointPublicPath] = true;

      assets[entryName] = [
        ...(assets[entryName] || []),
        entryPointPublicPath,
      ];
    });
  }

  return assets;
}

class OrchestratorManifestWebpackPlugin {
  constructor(userOptions = {}) {
    const defaultOptions = {
      filename: 'orchestrator-manifest.json',
      seed: {},
    };

    this.options = {
      ...defaultOptions,
      ...userOptions,
    };
  }

  apply(compiler) {
    const self = this;
    // convert absolute filename into relative so that webpack can
    // generate it at correct location
    const { filename } = this.options;
    if (path.resolve(filename) === path.normalize(filename)) {
      this.options.filename = path.relative(compiler.options.output.path, filename);
    }

    compiler.hooks.emit.tapAsync('OrchestratorManifestWebpackPlugin',
      (compilation, callback) => {
        // Get all entry point names for this module
        const entryNames = Array.from(compilation.entrypoints.keys());

        // Turn the entry point names into file paths
        const assets = getAssets(compilation, self.options.filename, entryNames, self.options.seed);

        // If this is a hot update compilation, move on!
        if (isHotUpdateCompilation(assets)) {
          return callback();
        }

        const assetJson = JSON.stringify(assets);
        const { filename: finalOutputName } = self.options;

        // eslint-disable-next-line no-param-reassign
        compilation.assets[finalOutputName] = {
          source: () => assetJson,
          size: () => assetJson.length,
        };

        return callback();
      });
  }
}

module.exports = OrchestratorManifestWebpackPlugin;
