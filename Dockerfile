FROM node:12.18.3-stretch

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git openssh-client gridsite-clients

ENV GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"

WORKDIR /usr/src/app

COPY yarn.lock package.json /usr/src/app/

RUN yarn

COPY . /usr/src/app
